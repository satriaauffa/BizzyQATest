# Welcome To Bizzy Quality Engineer Test   

You can choose your preferred programming language   
Please create your own branch with [branch name] = [your name]   


## 1. Basic Algorithm.
**1.1 Array Processing and Optimization**   
   Given two integer of arrays with same size. Find the same member between those two arrays and output it to the console   
   Example :   
   ```java
   int A = [1,2,3,4,5]
   int B = [1,3,5,7,9]
   output should be : 1 3 5
   ```
   Hint : value plus if you can solve it with optimized algorithm   

**1.2 Array assertion**   
   Given integer of arrays , please output **TRUE**, if the array is in ascending order, otherwise **FALSE**   
   Example :   
   ```java
   int A = [1,2,3,4] , output TRUE
   int B = [1,3,2,4] , output FALSE
   ```

**1.3 Array grouping**   
   Given string of arrays, please grouping / distinct the string with the number of occurences   
   Example :
   ```java
   String buah = ["apel","jeruk","apel","pepaya","pepaya","apel"]
   output should be similar like this:
      apel = 3
      jeruk = 1
      pepaya = 2
   ```

   
